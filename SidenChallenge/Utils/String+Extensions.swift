//
//  String+Extensions.swift
//  SidenChallenge
//
//  Created by Choudhary, Alok on 25/10/2020.
//  Copyright © 2020 Choudhary, Alok. All rights reserved.
//

import Foundation


extension String {
    
    func isAlphanumeric() -> Bool {
        return self.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) == nil && self != ""
    }
    
}
