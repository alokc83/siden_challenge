//
//  UserSplitViewController.swift
//  SidenChallenge
//
//  Created by Choudhary, Alok on 26/10/2020.
//  Copyright © 2020 Choudhary, Alok. All rights reserved.
//

import Foundation
import UIKit

class UserSplitViewController: UISplitViewController, UISplitViewControllerDelegate {
    
    override func viewDidLoad() {
        delegate = self
        preferredDisplayMode = .allVisible
    }

    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return true
    }
}
