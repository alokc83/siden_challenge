//
//  GitHubUserCell.swift
//  SidenChallenge
//
//  Created by Choudhary, Alok on 25/10/2020.
//  Copyright © 2020 Choudhary, Alok. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class GitHubUserCell:UITableViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var scoreValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupCell(dataModel:UserTableModel) {
        let url = URL(string: dataModel.avatarUrl)
        avatarImageView.kf.setImage(with: url)
        nameLabel.text = dataModel.name
        scoreValueLabel.text = String(dataModel.score)
        
    }
    
}
